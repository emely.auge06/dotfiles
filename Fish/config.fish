alias pride "figlet PRIDE | lolcat"
alias threat "cowsay Respect the trans dear cis and hets, or the Linux GNU will be your death | lolcat"
alias threat1 "cowsay Respect the enbys with their fries, or meet your inevatable demise | lolcat -t"
set -g spark_version 1.0.0

complete -xc spark -n __fish_use_subcommand -a --help -d "Show usage help"
complete -xc spark -n __fish_use_subcommand -a --version -d "$spark_version"
complete -xc spark -n __fish_use_subcommand -a --min -d "Minimum range value"
complete -xc spark -n __fish_use_subcommand -a --max -d "Maximum range value"

alias fc="nvim /home/loeng/.config/fish/config.fish"
alias pc="nvim /home/loeng/.config/picom.conf"
alias conf="nvim /home/loeng/.config/nvim/init.vim"
alias bar="seq 79 | sort --random-sort | spark | lolcat -a"
alias c="clear && bar"
alias fetch="bar && neofetch | lolcat -a && cpufetch | lolcat -a && bar"
alias cf="clear && fetch"
function mood
    fortune $argv | cowsay | lolcat && sl
end
function letters
    cat $argv | awk -vFS='' '{
        for(i=1;i<=NF;i++){
            if($i~/[a-zA-Z]/) {
                w[tolower($i)]++
            }
        }
    }END{
        for(i in w) print i,w[i]
    }' | sort | cut -c 3- | spark |lolcat
        printf '%s\n' 'abcdefghijklmnopqrstuvwxyz' '' | lolcat
end
neofetch | lolcat
fortune | cowsay | lolcat
